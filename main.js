const api_key = "9cf026c392f94f2488d8165b776512e2";

fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=${api_key}`)
    .then((resp) => resp.json())
    .then(function (data) {
        console.log(data);
        for (var i = 0; i < data.articles.length; i++) {
            if (i == 0) {
                $('#main-headline').append(
                    `
                    <div class="headline">
                        <img style="width:100%" src= "${data.articles[i].urlToImage}">
                        <h4>${data.articles[i].title}</h4>
                        <p class="small text-muted">${data.articles[i].author}</p>
                        <p>${data.articles[i].description}</p>
                    </div>
                    `
                )
            } else if (i >= 1 && i <= 2) {
                $('#side-list').append(
                    `
                    <div class='news'>
                    <div class="news-list-head>">
                    <img class="news-img" src="${data.articles[i].urlToImage}">
                    <h6 class="news-title">${data.articles[i].title}</h6>
                    </div><br class="clear-both">
                    <p class="small text-muted">${data.articles[i].author}</p>
                    <p>${data.articles[i].description}</p>
                    <hr>
                    </div>
                    `
                )
            } else {
                $('#next-list').append(
                    `
                    <div class='news'>
                    <div class="news-list-head>">
                    <img class="news-img" src="${data.articles[i].urlToImage}">
                    <h6 class="news-title">${data.articles[i].title}</h6>
                    </div><br class="clear-both">
                    <p class="small text-muted">${data.articles[i].author}</p>
                    <p>${data.articles[i].description}</p>
                    <hr>
                    </div>
                    `
                )
            }
        }
    })
    .catch(function () {
        console.log("Fetch Data Error");
    });

function CariBerita(){
var query = $('#cari-berita').val();
var url =  `https://newsapi.org/v2/everything?q=${query}&apiKey=${api_key}`;
fetch(url)
.then((resp) => resp.json())
.then(function (data) {
    console.log(data);
    $('#main-headline').empty();
    $('#side-list').empty();
    $('#next-list').empty();
    for (var i = 0; i < data.articles.length; i++) {
        if (i == 0) {
            $('#main-headline').append(
                `
                <div class="headline">
                    <img style="width:100%" src= "${data.articles[i].urlToImage}">
                    <h4>${data.articles[i].title}</h4>
                    <p class="small text-muted">${data.articles[i].author}</p>
                    <p>${data.articles[i].description}</p>
                </div>
                `
            )
        } else if (i >= 1 && i <= 2) {
            $('#side-list').append(
                `
                <div class='news'>
                <div class="news-list-head>">
                <img class="news-img" src="${data.articles[i].urlToImage}">
                <h6 class="news-title">${data.articles[i].title}</h6>
                </div><br class="clear-both">
                <p class="small text-muted">${data.articles[i].author}</p>
                <p>${data.articles[i].description}</p>
                <hr>
                </div>
                `
            )
        } else {
            $('#next-list').append(
                `
                <div class='news'>
                <div class="news-list-head>">
                <img class="news-img" src="${data.articles[i].urlToImage}">
                <h6 class="news-title">${data.articles[i].title}</h6>
                </div><br class="clear-both">
                <p class="small text-muted">${data.articles[i].author}</p>
                <p>${data.articles[i].description}</p>
                <hr>
                </div>
                `
            )
        }
    }
})
.catch(function () {
    console.log("Fetch Data Error");
});

}